var rows = 5;
var columns = 5;

var currPic;
var otherPic;

var turns = 0;

window.onload = function() {
  
    // initialisation du board en 5x5 avec des images blanches

    for (let r = 0; r < rows; r++) {
        for (let c = 0; c < columns; c++) {
            let tile = document.createElement("img");
            tile.src = "./images/blank.jpg";

            tile.addEventListener("dragstart", dragStart); //click sur l'image à glisser
            tile.addEventListener("dragover", dragOver);   //glisser l'image
            tile.addEventListener("dragenter", dragEnter); //faire glisser une image dans une autre
            tile.addEventListener("dragleave", dragLeave); //faire glisser une image hors d'une autre
            tile.addEventListener("drop", dragDrop);       //deposer une image sur une autre
            tile.addEventListener("dragend", dragEnd);     //après avoir deposer l'image

            document.getElementById("board").append(tile);
        }
    }

//initalisation du tableau qui va accueillir toutes les pièces
let pieces = [];

//fonction pour mettre les images de la tableau des pieces
for (let i = 1; i <= rows*columns; i++) {
    pieces.push(i.toString());
}

//mise en désordre des pièces
pieces.reverse();

//fonction pour inverser les images a chaque actualisation de page;
for (i = 0; i < pieces.length; i++) {
    let j = Math.floor(Math.random() * pieces.length);

    let tmp = pieces[i];
    pieces[i] = pieces[j];
    pieces[j] = tmp;
}

//fonction pour appeler l'image par son nom et l'integrer a la div pieces et
//ajouter à chaque image un evenement qui va nous permettre de faire glisser et les images et leurs cliquer dessus
for (let i = 0; i < pieces.length; i++) {
    let tile = document.createElement("img");
    tile.src = "./images/" + pieces[i] + ".jpg";
    
    
    tile.addEventListener("dragstart", dragStart); //click sur l'image à glisser
    tile.addEventListener("dragover", dragOver);   //glisser l'image
    tile.addEventListener("dragenter", dragEnter); //faire glisser une image dans une autre
    tile.addEventListener("dragleave", dragLeave); //faire glisser une image hors d'une autre
    tile.addEventListener("drop", dragDrop);       //deposer une image sur une autre
    tile.addEventListener("dragend", dragEnd);     //après avoir deposer l'image
    
    
    document.getElementById("pieces").append(tile);
}
}

//fonction qui definit l'element sur lequel j'appuie
function dragStart() {
    currTile = this; //ref à la piece qu'on a deja séléctionné
}

// cette fonction va empecher 
function dragOver(e) {
    e.preventDefault();
}

//????
function dragEnter(e) {
    e.preventDefault();
}

//????
function dragLeave() {

}

//????
function dragDrop() {
    otherTile = this;
}

// fonction qui place la piece et qui ajoute 1 a turns
function dragEnd() {
    if (currTile.src.includes("blank")) {
        return;
    }
    let currImg = currTile.src;
    let otherImg = otherTile.src;
    currTile.src = otherImg;
    otherTile.src = currImg;

    turns += 1;
    document.getElementById("turns").innerText = turns;
}